The Library of Babel
====================

A Rust implementation of [The Library of Babel](https://en.wikipedia.org/wiki/The_Library_of_Babel).

Usage
-----

Pages are identified in the library by the wall, shelf, volume, page number in base10, and room address in base64 separated by colons.

`wall:shelf:volume:page:hex_room`

Thanks to | Related Projects
----------------------------

<ul>
<li><a href="https://github.com/aldrio/library-of-babel">library-of-babel</a></li>
</ul>

License
-------
The license is `GNU General Public License v3.0`